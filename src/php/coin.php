<pre>
<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('includes/class.COIN.php');

// location private en public key:
$private_key = file_get_contents('includes/private-key.pem');
$public_key = file_get_contents('includes/public-key.pem');

// config:
define('HMAC_ENCRYPTED_SECRET', "PLAATS HIER HMAC SECRET");
define('RESTDOMAIN','dev-api.coin.nl');
define('RESTURL', 'https://dev-api.coin.nl');
define('RESTCONSUMER','PLAATS HIER CONSUMER USER');
openssl_private_decrypt(base64_decode(HMAC_ENCRYPTED_SECRET), $hmac_secret, $private_key);
define('HMAC_SECRET',$hmac_secret);

// Debug mode
$debug = false;

// Get /subscribers/v2/0252-340232
$api_method = 'GET';
$api_request = RESTURL."/subscribers/v2/0252-340232";
$api_result = COIN::restapi($private_key,$api_request,$api_method,NULL,$debug);

// Print JSON decoded result:
echo "\n\nResult for {$api_method}: {$api_request} \n";
print_r($api_result);

// PUT 
$api_method = 'PUT';
$api_body = '{"connectionKey":"MX221","networkType":"FixedNetworkConnection","phoneNumber":"0252517570","serviceProvider":"MAXITEL","usageType":"Voice","person":{"firstName":"J.","lastName":"SIERINK","prefix":""},"address":{"city":"BEINSDORP","houseNumber":"126","houseNumberAddition":"","postcode":"2144KD","street":"RIETKRAAG"},"listingPreferences":{"electronicDirectory":true,"numberMasking":false,"paperDirectory":true,"subscriberInformationServices":true}}';
$api_request = RESTURL."/subscribers/v2/";
$api_result = COIN::restapi($private_key,$api_request,$api_method,$api_body,$debug);

// Print JSON decoded result:
echo "\n\nResult for {$api_method}: {$api_request} \n";
print_r($api_result);
