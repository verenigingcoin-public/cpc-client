package nl.coin.ctp.sample;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * HMAC SHA256 signer
 */
public class HmacSha256Signer {
    private static final String HMAC_SHA_ALGORITHM = "HMacSHA256";
    private Mac signer;

    public HmacSha256Signer(String key) {
        try {
            SecretKey sk = new SecretKeySpec(key.getBytes("UTF-8"), HMAC_SHA_ALGORITHM);
            signer = Mac.getInstance(HMAC_SHA_ALGORITHM);
            signer.init(sk);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    public String sign(String data) {
        try {
            signer.reset();
            byte[] signature = signer.doFinal(data.getBytes("UTF-8"));
            return new String(Base64.encodeBase64(signature), "ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
