import connexion
from swagger_server.models.subscriber import Subscriber
from datetime import date, datetime
from typing import List, Dict
from six import iteritems
from ..util import deserialize_date, deserialize_datetime
from random import randint


def get_subscriber_for_phonenumber(PhoneNumber):
    """
    subscriber information for this telephone number
    
    :param PhoneNumber: 
    :type PhoneNumber: str

    :rtype: Subscriber
    """
    return Subscriber(phone_number=PhoneNumber, service_provider="KPN", first_name="Mark", prefix="van", last_name="Holsteijn", street="Tielweg", house_number="10", postcode="2803PK", city="Gouda", id=randint(1, 100000))
