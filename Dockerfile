FROM openjdk:8-jre-alpine

ADD target/lib /app/lib/
ADD target/sample-client.jar /app/

ENTRYPOINT [ "java" , "-jar", "/app/sample-client.jar" ]
