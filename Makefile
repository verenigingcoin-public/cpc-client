include Makefile.mk
IMAGE=$(NAME)

.PHONY: run-client update-key 

help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

test:
	mvn -Dversion=$(VERSION) -B clean test

pre-build:	
	mvn -Dversion=$(VERSION) -B clean package versions:display-dependency-updates

private-key.pem:
	ssh-keygen -m PEM -t rsa -b 4096 -f private-key.pem -N ''
	chmod 0600 private-key.pem

public-key.pem: private-key.pem
	ssh-keygen -e -m PKCS8 -f private-key.pem > public-key.pem

qa: sonarqube-java

run-client: docker-compose-up sharedkey.encrypted update-key	## generate keys and run sample java client
	docker run --network host -w /root -v $(PWD):/root $(IMAGE)

sharedkey.encrypted: docker-compose-up public-key.pem
	@curl -sS http://localhost:8001/consumers/sample-user/hmac-auth | \
		jq -r .data[0].secret | \
		tr -d '\n' | \
		openssl rsautl -encrypt -pubin -inkey public-key.pem | \
		base64 | \
		tr -d '\n' > \
		sharedkey.encrypted

update-key: JWT_KEY_ID=$(shell curl -sS -X GET http://localhost:8001/consumers/sample-user/jwt  | jq -r '.data[0].id')
update-key: DATA=$(shell jq -r -n --rawfile key public-key.pem '{"rsa_public_key": $$key, "algorithm":"RS256"}')
update-key: docker-compose-up
	@curl -sS -X PATCH http://localhost:8001/consumers/sample-user/jwt/$(JWT_KEY_ID) -H 'Content-Type: application/json' --data '$(DATA)'

docker-compose-up:						## start kong with sample service
	@test -n "$(shell docker-compose -f src/test/kong/docker-compose.yaml top sample-service)" || docker-compose -f src/test/kong/docker-compose.yaml up -d
	@while [ -z "$$(docker-compose -f src/test/kong/docker-compose.yaml top sample-service)" ]; do echo 'waiting for sample-service'; sleep 5 ; done

sonarqube-java:
	mvn -Dversion=$(VERSION) $(MVN_OPTIONS) -Dsonar.host.url=https://alm.coin.nl -B verify test sonar:sonar

dependency-vulnerability-check-java:
	mvn -Dversion=$(VERSION) $(MVN_OPTIONS) -B verify -DfailBuildOnCVSS=8

version-check-java:
	mvn -Dversion=$(VERSION) $(MVN_OPTIONS) -B versions:display-dependency-updates
